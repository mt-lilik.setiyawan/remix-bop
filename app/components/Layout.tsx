import {
  DesktopOutlined,
  FileOutlined,
  PieChartOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Link } from "@remix-run/react";
import type { MenuProps } from "antd";
import { Layout, Menu } from "antd";
import { useState } from "react";

const { Content, Sider } = Layout;

type MenuItem = Required<MenuProps>["items"][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[]
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem(<Link to="customers">Customer Info</Link>, "1", <PieChartOutlined />),
  getItem(<Link to="task-list">Task List</Link>, "2", <DesktopOutlined />),
  getItem("Escalation", "sub1", <UserOutlined />, [
    getItem("CDD", "3"),
    getItem("EDD", "4"),
  ]),
  getItem("Files", "9", <FileOutlined />),
];

type ChildProps = {
  children: React.ReactNode;
};

const AppLayout = (props: ChildProps) => {
  const [collapsed, setCollapsed] = useState(false);

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <Link to="/">
          <div
            style={{
              height: 32,
              margin: 16,
              background: "red",
              textAlign: "center",
              textDecoration: "none",
              color: "white",
              padding: 5
            }}
          >
            Back Office Portal
          </div>
        </Link>
        <Menu theme="dark" mode="inline" items={items} />
      </Sider>
      <Layout className="site-layout">
        <Content style={{ margin: "16px" }}>
          <div
            style={{
              padding: 20,
              height: "100%",
              background: "white",
            }}
          >
            {props.children}
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};

export default AppLayout;

import { Collapse } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";

const { Panel } = Collapse;

export default function CustomerDetail({ customerDetail }: any) {
  console.log(customerDetail);
  return (
    <div>
      <Collapse
        bordered={false}
        defaultActiveKey={["personal-information"]}
        expandIcon={({ isActive }) => (
          <CaretRightOutlined rotate={isActive ? 90 : 0} />
        )}
        className="site-collapse-custom-collapse"
        ghost
      >
        <Panel
          header="Personal Information"
          key="personal-information"
          className="site-collapse-custom-panel"
        >
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Customer ID</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.id}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>NIK/KTP Number</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.idNumber}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Fullname</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.fullName}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Place of Birth</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.placeOfBirth}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Date of Birth</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.dateOfBirth}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Gender</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.gender}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Marital Status</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.maritalStatus}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Phone Number</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.phoneNumber}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Email</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.email}</div>
          </div>
        </Panel>
        <Panel
          header="Correspondence Address"
          key="correspondence-address"
          className="site-collapse-custom-panel"
        >
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Address</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.correspondenceAddress}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Province</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.correspondenceProvince}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>City</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.correspondenceCity}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>District</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.correspondenceDistrict}</div>
          </div>
          <div style={{ display: "flex", alignItems: "center", padding: 5 }}>
            <div style={{ width: 150 }}>Sub District</div>
            <div style={{ width: 10 }}>:</div>
            <div>{customerDetail.correspondenceSubDistrict}</div>
          </div>
        </Panel>
      </Collapse>
    </div>
  );
}

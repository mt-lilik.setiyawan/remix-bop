import API from "./Api";

export async function getUserDetail(customerId: string) {
  try {
    const response = await API.client.get(
      `admin-portal/customer/${customerId}?enableCustomerPackage=1&enableLinkedPartners=true`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
}

export async function getCustomerPockets(customerId: string) {
  try {
    const response = await API.client.get(
      `account/admin/customers/${customerId}/accounts`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
}

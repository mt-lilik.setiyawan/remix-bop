import axios from "axios";

class Api {
  client = axios.create({
    baseURL: process.env.BASE_URL,
    headers: {
      "x-tyk-auth": process.env.TYK_AUTH,
      Authorization:
        `Bearer ${process.env.JWT_TOKEN}`
    },
  });
}

export default new Api();

import { createCookie } from "@remix-run/node"; // or cloudflare/deno

export const authCookies = createCookie("auth-cookies", {
  maxAge: 604_800, // one week
});
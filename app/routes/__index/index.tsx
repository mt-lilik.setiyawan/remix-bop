import type { ActionFunction, LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { Form, Link, useLoaderData } from "@remix-run/react";
// import AppLayout from "~/components/Layout";
import { destroySession, requireUserSession } from "~/services/session";

export let loader: LoaderFunction = async ({ request }) => {
  const session = await requireUserSession(request);
  return {
    token: session.get("token"),
  };
};

export const action: ActionFunction = async ({ request }) => {
  const session = await requireUserSession(request);
  return json(
    { message: "log out" },
    {
      headers: {
        "Set-Cookie": await destroySession(session),
      },
    }
  );
};

export default function DashboardPage() {
  const data = useLoaderData();
  return (
    <div>
      <h1>Welcome to Remix Protected Dashboard</h1>
      <p>TOKEN : {data?.token}</p>
      <Link to="/customers">Customers</Link>
      <Form method="post">
        <button>Log Out</button>
      </Form>
    </div>
  );
}

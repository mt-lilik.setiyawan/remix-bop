import type { LoaderArgs, LoaderFunction } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import { Table } from "antd";
import { requireUserSession } from "~/services/session";
import customerData from "../../../constants/customer.data.json";

export const loader: LoaderFunction = async ({ request }: LoaderArgs) => {
  await requireUserSession(request);
  const customerList = customerData.map((data, index) => {
    return {
      key: index,
      id: data.id,
      fullName: data.fullName,
    };
  });
  return { customerList };
};

export default function Customers() {
  const data = useLoaderData();

  const columns = [
    {
      title: "Customer Id",
      dataIndex: "id",
      key: "id",
      render: (id: string, record: any) => (
        <Link to={`${id}/detail`} state={record}>
          {id}
        </Link>
      ),
    },
    { title: "Name", dataIndex: "fullName", key: "name" },
  ];

  return (
    <>
      <div>Customers List</div>
      <div>
        <Table
          dataSource={data.customerList}
          columns={columns}
          size="small"
          bordered
        />
      </div>
    </>
  );
}

import type { LoaderArgs, LoaderFunction } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { Table } from "antd";
import { getCustomerPockets } from "~/datasources/customers.datasource";
import { requireUserSession } from "~/services/session";

export const loader: LoaderFunction = async ({
  request,
  params,
}: LoaderArgs) => {
  await requireUserSession(request);
  const customerId = params.customerId;
  if (customerId) {
    const accounts = await getCustomerPockets(customerId);
    return { pockets: accounts.data };
  }

  return { pockets: [] };
};

const columns = [
  {
    title: "Pocket Name",
    dataIndex: "aliasName",
    key: "aliasName",
  },
  {
    title: "Pocket ID",
    dataIndex: "accountNumber",
    key: "accountNumber",
  },
  {
    title: "Balance",
    dataIndex: "availableBalance",
    key: "availableBalance",
  },
  {
    title: "Linked Card",
    dataIndex: "",
    key: "cardLinking",
  },
  {
    title: "Pocket Status",
    dataIndex: "status",
    key: "status",
  },
];

export default function Pockets() {
  const loaderData = useLoaderData();
  return (
    <Table columns={columns} size="small" dataSource={loaderData.pockets} />
  );
}

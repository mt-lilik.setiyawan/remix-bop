import type { LoaderFunction, LoaderArgs } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { Outlet, useLoaderData, useNavigate } from "@remix-run/react";
import { Button, Tabs } from "antd";
import { getUserDetail } from "~/datasources/customers.datasource";
import CustomerDetailScreen from "../../../components/CustomerDetail";

export const loader: LoaderFunction = async ({ params }: LoaderArgs) => {
  const customerId = params.customerId;
  if (customerId) {
    const customerData = await getUserDetail(customerId);
    return { customerId, customerDetail: customerData.data };
  }

  return redirect("/customers");
};

export function ErrorBoundary() {
  return <>There is error when show customer detail</>;
}

export default function CustomerDetail() {
  const { customerDetail } = useLoaderData();

  const navigate = useNavigate();

  const handleClick = (key: any) => {
    navigate(key);
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <div>
          <h3>{customerDetail.fullName}</h3>
        </div>
        <div>
          <Button type="primary" danger>
            Close Customer
          </Button>
        </div>
      </div>

      <div>
        <Tabs
          defaultActiveKey="1"
          onChange={handleClick}
          items={[
            {
              label: `Customer Detail`,
              key: "detail",
              children: <CustomerDetailScreen customerDetail={customerDetail} />,
            },
            {
              label: `Pockets`,
              key: "pockets",
              children: <Outlet />,
            },
            {
              label: `Manual Adjustment`,
              key: "3",
              children: `Content of Tab Pane 3`,
            },
          ]}
        />
      </div>
    </div>
  );
}

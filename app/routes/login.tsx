import type { ActionFunction } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { Form, useActionData } from "@remix-run/react";
import { Button, Input } from "antd";
import { commitSession, getSession } from "~/services/session";

export const action: ActionFunction = async ({ request }) => {
  // check the input form
  const forms = await request.formData();
  const inputToken = forms.get("token") || null;

  if (inputToken) {
    const cookieHeader = request.headers.get("Cookie");
    const session = await getSession(cookieHeader);
    session.set("token", inputToken);
    session.set("isLoggedIn", true);

    return redirect("/", {
      headers: {
        "Set-Cookie": await commitSession(session),
      },
    });
  }

  return {
    message: "Please input the token",
  };
};

export default function Login() {
  const actionData = useActionData();

  const { TextArea } = Input;

  return (
    <div style={{ paddingTop: "10%" }}>
      <center>
        <div>Login Screens</div>
        <div style={{ margin: "0px 100px" }}>
          <Form method="post">
            <div>
              <TextArea name="token" />
            </div>
            <div>
              <Button htmlType="submit" type="primary">Login</Button>
            </div>
            <div>{actionData?.message || ""}</div>
          </Form>
        </div>
      </center>
    </div>
  );
}

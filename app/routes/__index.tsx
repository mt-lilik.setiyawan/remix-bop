import { Outlet } from "@remix-run/react";
import AppLayout from "~/components/Layout";

export default function MainLayout() {
  return (
    <>
      <AppLayout>
        <Outlet />
      </AppLayout>
    </>
  );
}
